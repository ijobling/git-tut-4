@annotation:tour intro
#A POETIC INTRODUCTION TO GIT

#Module 4 - The HEAD state, Git Log and Git Diff
This is a very short module and teaches you how to look up old commits in the Git history logs.

This is going to be very helpful when looking at our next module.

@annotation:tour refer
#Referring to Commits
Each time you perform a `git commit` Git records the main data in its logs.

```
git log
```

and you’ll see all commits listed in the log, which will look something like 

```
commit 5d6d86cacc4ba5ce1e1e9ee23ca86defd6884a62
Author: fmay <fmay@applicationcraft.com>
Date:   Thu Jul 17 09:02:21 2014 +0000  

    Added END marker  
    
commit 416e70ff4380e07c744f18d7804919b6b412042f 
Merge: 0b9c0d8 6c8e454
Author: fmay <fmay@applicationcraft.com>
Date:   Mon Jul 14 17:49:43 2014 +0000

    fixed conflict 
```

- **commit** is the ‘SHA’ id which uniquely identifies each individual commit (more on this soon)
- **Author** is the person who made the commit. This is especially useful when you are collaborating with other users using systems like BitBucket or GitHub.
- **Date** is obvious
- **Description** is the message you added when you ran `commit -m`

@annotation:tour SHA
#The SHA identifier
Here's a commit log entry again

```
commit 5d6d86cacc4ba5ce1e1e9ee23ca86defd6884a62                                                                                 
Author: fmay <fmay@applicationcraft.com>                                                                                        
Date:   Thu Jul 17 09:02:21 2014 +0000                                                                                          

    Added END marker
```

Each commit has an SHA ID that looks something like 

```
eba6d9b7648f07e0a9da3140a0fbc1344860e524
```

Many git commands (for example `git reset` and `git revert`) require you specify a commit ID as a parameter.

```
git reset --hard eba339 (don't run this)
```

You only need to use the first few characters, though, as Git will check the uniqueness and complain if it needs more. 

You need to use enough characters to still be uniquely identifiable to Git, so try 4 or 5 characters. If Git complains you'll need to use more.

The above command shows us just using 6 characters of the SHA id. It resets everything to the point in time of that commit and kills all subsequent commits. We'll be covering this in the next module.


@annotation:tour whatisHEAD
#What is the HEAD?

`HEAD` is a pointer to the most recent commit in the *active* branch. 

Let's visualize what we have in our repo. As you can see from the image below, we have 3 branches (master, fixv3, sonnets).

![](img/logs-master.png) 

The `master` branch is currently selected and the `HEAD` points to the most recent commit within that branch. You can see the other two branches parked on the side.

![](img/logs-sonnets.png) 

The above image shows what happens if we have checked out the `sonnets` branch. You can see that there are 4 commits within this branch. `master` and `fixv3` are parked and the `HEAD` points to the most recent commit within the `sonnets` branch.

![](img/logs-fixv3.png)

Finally, the image above  shows what happens if we have checked out the `fixv3` branch.

Notice how in each case you can see which commit (Verse 3 in both cases) our `sonnets` and `fixv3` branches are branched from. 

@annotation:tour headexamples
#Examples of where you might use HEAD
You will often want to refer to a recent commit rather than reference its SHA id. 

Let’s say you want to remove changes made by a recent commit

- `git revert HEAD` - removes changes introduced in the most recent commit 
- `git revert HEAD~2` - removes changes introduced in the 3rd most recent commit 
- `git revert HEAD^^` - also removes changes introduced in the 3rd most recent commit, where each ‘^’ character represents one commit back from HEAD.

In the next module, we’ll be referring to commits a lot, so make sure you feel comfortable with the `HEAD` concept.

@annotation:tour scroll
#Scrolling through commits
Start off by viewing the log in your terminal window using

```
git log
```

You can use the arrow keys to scroll up and down through the commit log. Once you are done, you can press ‘q’.

@annotation:tour search
Section - Searching commits
If you want to locate a specific commit by some text contained within the message, try this

```
git log —grep=searchstring
```

@annotation:tour diffintro
#Seeing changes between commits : git diff
At some point, you will want to see what changed between commits. The `git diff` command lets you see this at either the commit level or the file level.

Let's look at some use cases.

@annotation:tour basic
#Use Case : vanilla git diff
If you run the following command

```
git diff
```

It will show you the difference between the working directory changes not yet staged and staged files ready for commit.

```
diff --git a/mary.txt b/mary.txt  
index 9db5031..cc325ef 100644
--- a/mary.txt
+++ b/mary.txt
@@ -12,3 +12,5 @@ It followd her to school one day
 School one day, school one day
 It followed her to school one day
 Which was against the rules.
+
+Cannot think of another verse     
```

- The file `mary.txt` was modified
- Lines starting with a '+' character tell us what was added ("Cannot think of another verse")
- Lines starting with a '-' character tell us what was removed (nothing in this case)


@annotation:tour cached
#Use Case : changes between tracked files and your last commit
The following command would show you the changes made between your tracked files and the last commit.

In other words what you would be committing if you run "git commit" without "-a" option.

```
git diff --cached
```

@annotation:tour diffhead
#Use Case : changes between your working directory and the last commit
The following command shows you all changes made between your working directory and the last commit. Changes to untracked files are excluded.

In other words what you would be committing if you run "git commit -a".

```
git diff HEAD
```

@annotation:tour diffhead2
#Use Case : changes between your working directory and the last commit for a specific file
The following command shows you all changes made between your working directory and the last commit but for a named file. Changes to untracked files are excluded.

```
git diff HEAD mary.txt
```

@annotation:tour diffhead3
#Use Case : difference between named commits
The following command shows you all changes made between 2 named commits.

```
git diff HEAD HEAD^
```

or for a named file 

```
git diff HEAD HEAD^ mary.txt
```

@annotation:tour diffbranch
#Use Case : other branches
You might want to look for the differences between the HEAD position of your current branch and the HEAD position in a named branch.

```
git diff sonnets
```

@annotation:tour diffcommits
#Use Case : changes between named commits
You can look at the differences between 2 named commits by referencing their SHA IDs. This can span different branches of course.

```
git diff abc123 def456
```

or limited to a named file

```
git diff abc123 def456 mary.txt
```

@annotation:tour play
#Playtime
Use the current project to play around with the `git log` and `git diff`. 

Once you're completely comfortable with these, you're ready to look at the final, more advanced modules.






