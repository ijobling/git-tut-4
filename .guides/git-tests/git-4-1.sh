#!/bin/bash
QCOUNT=2

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
      1 )
				expect "Create a master.txt file in the dummy folder" "master.txt" "ls /home/codio/workspace/dummy"
				;;
      2 )
				expect "Commit the changes adding the requested message" "branches challenge 1" "git log -1"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command