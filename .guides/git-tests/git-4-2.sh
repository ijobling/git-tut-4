#!/bin/bash
QCOUNT=4

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
      1 )
				expect "Create a branch dummy-master" "dummy-master" "git branch"
				;;
      2 )
				expect "Checkout to dummy-master" "\*[[:space:]]*dummy-master" "git branch"
				;;
      3 )
				expect "Add a file dummy-1.txt to the dummy folder" "dummy-1.txt" "ls /home/codio/workspace/dummy/"
				;;
      4 )
				expect "Commit the changes adding the requested message" "dummy-master 1" "git log -1"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command