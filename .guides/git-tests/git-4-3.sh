#!/bin/bash
QCOUNT=5

# Run test
function test_command {
	(( COUNT ++ ))
	if [[ $COUNT -le $QCOUNT ]]; then
		case $COUNT in
      1 )
				expect "Make sure you are on the dummy-master branch" "\*[[:space:]]*dummy-master" "git branch"
				;;
      2 )
				expect "Create a branch dummy-1" "dummy-1" "git branch"
				;;
      3 )
				expect "Add a file dummy-2.txt to the dummy folder" "dummy-2.txt" "ls /home/codio/workspace/dummy/"
				;;
      4 )
				expect "Commit the changes adding the requested message" "dummy-master 2" "git log -1"
				;;
      5 )
				expect "Create a branch dummy-2" "dummy-2" "git branch"
				;;
		esac
	else		
		echo -e "Well done!"
		return 0
	fi
}


test_command