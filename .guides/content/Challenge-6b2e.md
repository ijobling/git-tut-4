By the end of this set of challenges, executing a `git branch -v` from the `master` branch should output the following (SHA id's may differ):

``` 
  dummy-1        2gdh467 dummy-master 1
  dummy-2        76dhye6 dummy-master 2
  dummy-master   6dgh46g dummy-master 2
* master         34dh564 [ahead N] branches challenge 1
```

The code-block above introduces a `dummy-master` branch created from `master`, a `dummy-1` and a `dummy-2` branch created from `dummy-master`.

On the other hand, the `[ahead N]` indicates that the remote Git repo, in this case `origin/master` is behind the local repo, so Git suggests to update the remote repo by pushing the latest commits.

Let's begin.

{Check It!|assessment}(test-1593853214)

|||guidance

### Correct answers:

1. Create a file `master.txt` in the `dummy` folder
2. `git add -A`, `git commit -m "branches challenge 1"`

|||