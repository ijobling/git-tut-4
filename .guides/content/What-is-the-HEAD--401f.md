`HEAD` is a pointer to the most recent commit in the *active* branch. 

Let's visualize what we have in our repo. As you can see from the image below, we have 2 branches that were created from `master`: `fixv3` and `sonnets`.

![](.guides/img/git-branches.png)

The `master` branch is currently selected and the `HEAD` points to the most recent commit within that branch. You can see the other two branches parked on the side.

![](.guides/img/git-branches-sonnets.png)

The above image shows what happens if we have checked out the `sonnets` branch. You can see that there are 2 commits within this branch. `master` and `fixv3` are parked and the `HEAD` points to the most recent commit within the `sonnets` branch.

![](.guides/img/git-branches-fixv3.png)

Finally, the image above  shows what happens if we have checked out the `fixv3` branch.

Notice how in each case you can see which commit our `sonnets` and `fixv3` branches are branched from.

---
Complete a set of challenges that'll let you master Git branching. Go to the next section.