## Changes between your working directory and the last commit for a specific file

The following command shows you all changes made between your working directory and the last commit but for a named file. Changes to untracked files are excluded.

```
git diff HEAD mary.txt
```