## Changes between tracked files and your last commit

The following command would show you the changes made between your tracked files and the last commit.

In other words what you would be committing if you've already run `git add -A`.

Try staging the changes in the `mary.txt` file from the previous section and then execute a:

```bash
git diff --cached
```

This time, executing a `git diff` may not display anything (unless there are still untracked files) because `--cached` means that the files are already in the _cache_ or _Git temporary memory_ of staged files.