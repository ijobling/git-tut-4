## Seeing changes between commits

At some point, you will want to see what changed between commits. The `git diff` command lets you see this at either the commit level or the file level.

Let's look at some use cases.

