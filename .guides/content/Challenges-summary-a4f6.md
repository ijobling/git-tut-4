[Click here to close files opened in previous section](close_file dummy/dummy-2.txt)

If you scored "Well done!" in all the previous challenges, then you will see the `dummy` folder files in the file tree changing in the following way:

- `git checkout master` will display the `master.txt` file
- `git checkout dummy-master` will display the `master.txt`, `dummy-1.txt` and the `dummy-2.txt` files
- `git checkout dummy-1` will display the `master.txt`, and the `dummy-1.txt` files
- `git checkout dummy-2` will display the `master.txt`, `dummy-1.txt` and the `dummy-2.txt` files

