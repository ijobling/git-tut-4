Each time you perform a `git commit` Git records the main data in its logs.

```
git log
```

and you’ll see all commits listed in the log, which will look something like 

```
commit 5d6d86cacc4ba5ce1e1e9ee23ca86defd6884a62
Author: username <user@email.com>
Date:   Thu Jul 17 09:02:21 2014 +0000  

    This is a commit message
    
commit 416e70ff4380e07c744f18d7804919b6b412042f 
Merge: 0b9c0d8 6c8e454
Author: username <user@email.com>
Date:   Mon Jul 14 17:49:43 2014 +0000

    Another commit message
```

- **commit** is the ‘SHA’ id which uniquely identifies each individual commit (more on this soon)
- **Author** is the person who made the commit. This is especially useful when you are collaborating with other users using systems like BitBucket or GitHub.
- **Date** is obvious
- **Description** is the message you added when you ran `commit -m`
