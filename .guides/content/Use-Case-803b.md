## Changes between your working directory and the last commit

Similar to `git diff --cached`, the following command shows you all changes made between your working directory and the last commit. Changes to untracked files are excluded.

In other words what you would be committing if you run `git add -A`.

```
git diff HEAD
```