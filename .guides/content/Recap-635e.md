In the previous unit you became familiar with Git branches.

Branching is one of Git’s most powerful features. Working in branches allows you to fully isolate each bug or feature you’re working on from your master branch and from other features and bug fixes you may be working on.

### Useful commands

|**Command**|**Description**|
|-----------|---------------|
|`git branch` | Display existing branches (the branch marked with a `*` indicates the active branch) |
|`git branch <name of the branch>` | Create a new branch (new branches inherit the last commit) |
|`git branch master <name of the branch>` | Create a new branch specifically from the `master` branch |
|`git checkout <branch-name>` | Switch to a branch  |
|`git branch -D <branch-name>` | Delete a branch |