Here's a commit log entry again

```
commit 345dgtecacc4ba5ce1e1e9ee23ca86defd6884a62
Author: username <user@email.com>
Date:   Thu Jul 17 09:02:21 2014 +0000

    Commit message
```

Each commit has an SHA ID that looks something like 

```
9ee23c7648f07e0a9da3140a0fbc1344860e524
```

Many git commands (for example `git reset` and `git revert`) require you specify a commit ID as a parameter.

```bash
git reset --hard 9ee23c
```

You only need to use the first few characters, though, as Git will check the uniqueness and complain if it needs more. 

You need to use enough characters to still be uniquely identifiable to Git, so try 4 or 5 characters. If Git complains you'll need to use more.

The above command shows us just using 6 characters of the SHA id. It resets everything to the point in time of that commit and kills all subsequent commits. We'll be covering this in the next unit.

|||info

### What does SHA stand for?

In case you are curious, the __Secure Hash Algorithm__ produces a hash value and it is typically rendered as a hexadecimal number 40 digits long, which Git uses for generating unique id's for the commits.

|||