Start off by viewing the log in your terminal window using

```
git log
```

You can use the arrow keys to scroll up and down through the commit log. Once you are done, you can press ‘q’.