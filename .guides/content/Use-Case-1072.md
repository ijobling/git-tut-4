## Other branches

You might want to look for the differences between the HEAD position of your current branch and the HEAD position in a named branch.

```
git diff dummy-master
```