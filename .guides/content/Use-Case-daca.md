## Vanilla git diff

Add the following line to the end of `mary.txt`: _Cannot think of another verse_

If you run the following command

```
git diff
```

It will show you the difference between the working directory changes not yet staged and staged files ready for commit.


```
diff --git a/mary.txt b/mary.txt  
index 9db5031..cc325ef 100644
--- a/mary.txt
+++ b/mary.txt
@@ -12,3 +12,5 @@ It followd her to school one day
 School one day, school one day
 It followed her to school one day
 Which was against the rules.
+
+Cannot think of another verse     
```

- The file `mary.txt` was modified
- Lines starting with a '+' character tell us what was added ("Cannot think of another verse")
- Lines starting with a '-' character tell us what was removed (nothing in this case)