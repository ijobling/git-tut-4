## Examples of where you might use HEAD

Now that you've experimented with how commits and branches work, let's _navigate_ through commits using `HEAD`

You will often want to refer to a recent commit rather than reference its SHA id.

Let’s say you want to remove changes made by a recent commit

- `git revert HEAD` - removes changes introduced in the most recent commit 
- `git revert HEAD~2` - removes changes introduced in the 3rd most recent commit 
- `git revert HEAD^^` - also removes changes introduced in the 3rd most recent commit, where each ‘^’ character represents one commit back from HEAD.

In the next unit, we’ll be referring to commits a lot, so make sure you feel comfortable with the `HEAD` concept.