If you want to locate a specific commit by some text contained within the message, try this

```
git log --grep=searchstring
```

For example, try finding the commit message: "dummy-master 2" using the command above:

```bash
git log --grep="dummy-master 2"
```

If nothing is returned it means that one of the previous challenges had a commit message that does not match the search string.