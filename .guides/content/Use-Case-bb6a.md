## Difference between named commits

The following command shows you all changes made between 2 named commits.

```
git diff HEAD HEAD^
```

or for a named file 

```
git diff HEAD HEAD^ mary.txt
```