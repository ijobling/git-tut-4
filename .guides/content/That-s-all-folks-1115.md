In this unit, you have deepened your knowledge about Git commits and branches.

It is possible to look at the differences between _staged_ files using the commit SHA id's and the `git diff` command.

In the next unit, you will learn how to undo changes in a specific file.