## Changes between named commits

You can look at the differences between 2 named commits by referencing their SHA IDs. This can span different branches of course.

```
git diff abc123 def456
```

or limited to a named file

```
git diff abc123 def456 mary.txt
```

---
Note: The SHA id's represented above are just for reference, thus this code won't work in your terminal window. You can try to make some file changes and then play with the real SHA id's of your commits displayed with `git log`.