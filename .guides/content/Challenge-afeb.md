|||warning

Files that are created or opened explicitly by you are not automatically closed. We would recommend you close these before proceeding.

[Click here to close these files now](close_file dummy/master.txt)

|||

{Check It!|assessment}(test-1698180752)

|||guidance

### Correct answers:

1. `git branch dummy-master`
2. `git checkout dummy-master`
3. Create file `dummy-1.txt` in the `dummy` folder
4. `git add -A`, `git commit -m "dummy-master 1"`

|||