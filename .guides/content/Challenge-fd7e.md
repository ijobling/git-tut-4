[Click here to close files opened in previous section](close_file dummy/dummy-1.txt)

{Check It!|assessment}(test-3155614997)

|||guidance

### Correct answers: 

1. `git branch dummy-1`
2. Create the file `dummy-2.txt` in the `dummy` folder (make sure you are in the `dummy-master` branch)
3. `git add -A`, `git commit -m "dummy-master 2"`
5. `git branch dummy-2`

|||